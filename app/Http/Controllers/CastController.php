<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required',
            'bio'   => 'required'
        ]);
        $query = DB::table('casts')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio"   => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $post = DB::table('casts')->get();
        return view('cast.index', compact('post'));
    }

    
    public function show($id)
    {
        $ambil = DB::table('casts')->where('id', $id)->first();
        return view('cast.show', compact('ambil'));
    }

    public function edit($id)
    {
        $post = DB::table('casts')->where('id', $id)->first();
        return view('cast.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'   => 'required'
        ]);

        $query = DB::table('casts')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
