<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToPeransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perans', function (Blueprint $table) {
            $table->foreign('film_id')->on('films')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cast_id')->on('casts')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perans', function (Blueprint $table) {
            //
        });
    }
}
